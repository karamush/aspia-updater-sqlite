<?php
//
// Aspia Project
// Copyright (C) 2020 Dmitry Chapyshev <dmitry@aspia.ru>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//

include 'config.php';

function getPackageId(SQLite3 $db, $package)
{
    $sql = "SELECT id FROM packages WHERE name='$package'";

    if (!$packageId = $db->querySingle($sql)) {
        die('Package not found.');
    }

    return $packageId;
}

function getUpdates(SQLite3 $db, $package_id, $version)
{
    $pieces = explode(".", $version);

    // We only support 3 groups of digits in the version number.
    while (count($pieces) > 3)
        array_pop($pieces);

    $version = implode(".", $pieces);

    $sql = "SELECT target_version, description, url
            FROM updates
            WHERE package_id = '$package_id' AND source_version = '$version'";

    if (!$result = $db->query($sql)) {
        die('Failed to execute database query: ' . $db->lastErrorMsg());
    }

    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    echo "<update>";

    if ($result->numColumns() != 0) {
        // There is an update available.
        $row = $result->fetchArray();

        echo "<version>" . $row['target_version'] . "</version>";
        echo "<description>" . $row['description'] . "</description>";
        echo "<url>" . $row['url'] . "</url>";
    }

    echo "</update>";
}

function doWork()
{
    parse_str($_SERVER["QUERY_STRING"], $query);

    // setup database
    $db = new SQLite3(Config::$db_file, SQLITE3_OPEN_READONLY);

    // Get the package name and version from the query.
    $package = SQLite3::escapeString($query['package']);
    $version = SQLite3::escapeString($query['version']);

    if (empty($package) || empty($version)) {
        die('Invalid request received.');
    }

    getUpdates($db,
        getPackageId($db, $package),
        $version);

    $db->close();
}

// Run the update check.
doWork();