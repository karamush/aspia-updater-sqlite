BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "users"
(
    id       INTEGER PRIMARY KEY AUTOINCREMENT,
    name     TEXT NOT NULL,
    password TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "packages"
(
    id   INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL UNIQUE
);
CREATE TABLE IF NOT EXISTS "updates"
(
    id         INTEGER PRIMARY KEY AUTOINCREMENT,
    package_id INTEGER,
    source_version,
    target_version,
    description,
    url
);

INSERT INTO "packages"
VALUES (1, 'console');
INSERT INTO "packages"
VALUES (2, 'host');
COMMIT;
